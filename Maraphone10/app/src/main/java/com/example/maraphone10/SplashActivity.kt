package com.example.maraphone10

import android.content.Intent
import android.media.Image
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import kotlin.random.Random


class VanishActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splash_activity)
        val rnd = Random.nextInt(0,4)
        when(rnd) {
            0 -> findViewById<ImageView>(R.id.imageView2).setImageResource(R.drawable.img_loading_screen)
            1 -> findViewById<ImageView>(R.id.imageView2).setImageResource(R.drawable.img2)
            2 -> findViewById<ImageView>(R.id.imageView2).setImageResource(R.drawable.img3)
            3 -> findViewById<ImageView>(R.id.imageView2).setImageResource(R.drawable.img4)
        }
        Handler(Looper.getMainLooper()).postDelayed({
            Intent(this, MainActivity::class.java).also {
                startActivity(it)
            }
        }, 3000)

    }
}