package com.example.maraphone10

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.icu.text.IDNA
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.PersistableBundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import java.io.Serializable
import kotlin.random.Random


class MainActivity : AppCompatActivity() {
    private val human = Person()
    private var numBackground = 3
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        loadPersonData()
        supportActionBar?.title = "Info"
    }

    override fun onSaveInstanceState(outState: Bundle) {
        setDataFromEditTextToPerson(human)
        super.onSaveInstanceState(outState)
        outState.putSerializable("HUMAN_DATA", human)

    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        val user = savedInstanceState.getSerializable("HUMAN_DATA") as Person
        setDataFromPersonToEditText(user)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.save -> savePersonData()
            R.id.help -> {
                val intentToDo = Intent(this, HelpActivity::class.java)
                startActivity(intentToDo)
            }
            R.id.changeBgr -> {
                fun getRandomIntExpectOne(num: Int): Int {
                    var n = Random.nextInt(0, 4)
                    while(num == n)
                        n = Random.nextInt(0, 4)
                    return n
                }

                val layout = findViewById<ConstraintLayout>(R.id.cLayout)
                val rnd = getRandomIntExpectOne(numBackground)
                when(rnd) {
                    0 -> layout.setBackgroundResource(R.drawable.img2)
                    1 -> layout.setBackgroundResource(R.drawable.img3)
                    2 -> layout.setBackgroundResource(R.drawable.img4)
                    3 -> layout.setBackgroundColor(Color.parseColor("#ffffff") )
                }
                numBackground = rnd
            }
        }
        return true
    }

    private fun savePersonData() {
        setDataFromEditTextToPerson(human)
        val sharedPreferences = getSharedPreferences("sharedPrefs", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.apply() {
            putString("HUMAN_NAME", human.name)
            putString("HUMAN_SECONDNAME", human.secondName)
            putString("HUMAN_FATHERSNAME", human.fathersName)
            putString("HUMAN_AGE", human.age)
            putString("HUMAN_HOBBY", human.hobby)
        }.apply()
    }

    private fun loadPersonData() {
        val sharedPreferences = getSharedPreferences("sharedPrefs", Context.MODE_PRIVATE)
        findViewById<EditText>(R.id.editTextName).setText(sharedPreferences.getString("HUMAN_NAME", null), TextView.BufferType.EDITABLE)
        findViewById<EditText>(R.id.editTextSecondName).setText(sharedPreferences.getString("HUMAN_SECONDNAME", null), TextView.BufferType.EDITABLE)
        findViewById<EditText>(R.id.editTextFathersName).setText(sharedPreferences.getString("HUMAN_FATHERSNAME", null), TextView.BufferType.EDITABLE)
        findViewById<EditText>(R.id.editTextAge).setText(sharedPreferences.getString("HUMAN_AGE", null), TextView.BufferType.EDITABLE)
        findViewById<EditText>(R.id.editTextHobby).setText(sharedPreferences.getString("HUMAN_HOBBY", null), TextView.BufferType.EDITABLE)
    }

    private fun setDataFromEditTextToPerson(human: Person) {
        human.name = findViewById<EditText>(R.id.editTextName).getText().toString()
        human.secondName = findViewById<EditText>(R.id.editTextSecondName).text.toString()
        human.fathersName = findViewById<EditText>(R.id.editTextFathersName).text.toString()
        human.age = findViewById<EditText>(R.id.editTextAge).text.toString()
        human.hobby = findViewById<EditText>(R.id.editTextHobby).text.toString()
    }

    private fun setDataFromPersonToEditText(human: Person) {
        findViewById<EditText>(R.id.editTextName).setText(human.name, TextView.BufferType.EDITABLE)
        findViewById<EditText>(R.id.editTextSecondName).setText(human.secondName, TextView.BufferType.EDITABLE)
        findViewById<EditText>(R.id.editTextFathersName).setText(human.fathersName, TextView.BufferType.EDITABLE)
        findViewById<EditText>(R.id.editTextAge).setText(human.age, TextView.BufferType.EDITABLE)
        findViewById<EditText>(R.id.editTextHobby).setText(human.hobby, TextView.BufferType.EDITABLE)
    }
    fun btnGoToInfoActivityonClick(view: View) {
        setDataFromEditTextToPerson(human)
        val intentToDo = Intent(this, InfoActivity::class.java)
        intentToDo.putExtra("HUMAN_DATA", human)
        startActivity(intentToDo)

        /*Intent(this, InfoActivity::class.java).also {
            startActivity(it)
        }*/

    }
}