package com.example.maraphone10

import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider

class HelpActivity : AppCompatActivity() {
    lateinit var viewModel: TestViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.help_activity)
        viewModel = ViewModelProvider(this).get(TestViewModel::class.java)
        viewModel.currentText.observe(this, Observer {
            findViewById<TextView>(R.id.textView2).setText(it)
        })
    }

    fun onClick(view: View) {
        viewModel.currentText.value = findViewById<EditText>(R.id.editTextTextPersonName).text.toString()
    }




}