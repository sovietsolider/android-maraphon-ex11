package com.example.maraphone10

import java.io.Serializable

class Person(_name: String = "Unknown", _secondName: String = "Unknown", _fathersName: String = "Unknown", _hobby: String = "Unknown", _age: String = "Unknown"): Serializable {
    var name: String = _name
    var secondName: String = _secondName
    var fathersName: String = _fathersName
    var hobby: String = _hobby
    var age: String = _age

}