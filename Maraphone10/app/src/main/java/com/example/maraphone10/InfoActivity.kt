package com.example.maraphone10

import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class InfoActivity : AppCompatActivity() {
    var human = Person()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.info_activity)

        human = intent.getSerializableExtra("HUMAN_DATA") as Person
        findViewById<TextView>(R.id.textViewName).text = getString(R.string.name_info, human.name)
        findViewById<TextView>(R.id.textViewName3).text = getString(R.string.secondname_info, human.secondName)
        findViewById<TextView>(R.id.textViewName4).text = getString(R.string.fathersname_info, human.fathersName)
        findViewById<TextView>(R.id.textViewName5).text = getString(R.string.age_info, human.age.toString())
        findViewById<TextView>(R.id.textViewName6).text = getString(R.string.hobby_info, human.hobby)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putSerializable("HUMAN_SAVE_DATA", human)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        val user = savedInstanceState.getSerializable("HUMAN_SAVE_DATA") as Person
        findViewById<TextView>(R.id.textViewName).text = getString(R.string.name_info, user.name)
        findViewById<TextView>(R.id.textViewName3).text = getString(R.string.secondname_info, user.secondName)
        findViewById<TextView>(R.id.textViewName4).text = getString(R.string.fathersname_info, user.fathersName)
        findViewById<TextView>(R.id.textViewName5).text = getString(R.string.age_info, user.age.toString())
        findViewById<TextView>(R.id.textViewName6).text = getString(R.string.hobby_info, user.hobby)

    }
}