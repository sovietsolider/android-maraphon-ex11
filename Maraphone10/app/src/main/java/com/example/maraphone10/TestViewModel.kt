package com.example.maraphone10

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class TestViewModel: ViewModel(){
    var text = ""

    val currentText: MutableLiveData<String> by lazy {
        MutableLiveData<String>()
    }

}